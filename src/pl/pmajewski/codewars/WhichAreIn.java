package pl.pmajewski.codewars;

import java.util.Arrays;
import java.util.stream.Collectors;

public class WhichAreIn {

	public static String[] inArray(String[] array1, String[] array2) {
		return Arrays.stream(array1).filter(i -> {
			return Arrays.stream(array2).filter(j -> {
				return j.contains(i);
			}).findFirst().isPresent();
		}).distinct().collect(Collectors.toList()).toArray(new String[0]);
	}
}
