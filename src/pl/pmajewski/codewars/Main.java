package pl.pmajewski.codewars;

import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		String a[] = new String[] { "arp", "live", "strong" };
		String b[] = new String[] { "lively", "alive", "harp", "sharp", "armstrong" };
		String r[] = new String[] { "arp", "live", "strong" };
		if(r.equals(WhichAreIn.inArray(a, b))) {
			System.out.println(":)");
		} else {
			System.out.println(":(");
			System.out.println("r: "+Arrays.toString(r));
		}
	}
}
